defmodule InpowerProto.MediaResponse.MediaType do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3
  @type t :: integer | :IMAGE | :VIDEO

  field :IMAGE, 0

  field :VIDEO, 1
end

defmodule InpowerProto.ListPostsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          page_no: integer,
          per_page: integer
        }

  defstruct [:page_no, :per_page]

  field :page_no, 1, type: :int32
  field :per_page, 2, type: :int32
end

defmodule InpowerProto.CreateRequestInput do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_id: String.t(),
          group_id: [String.t()],
          content: String.t(),
          sensitive: boolean,
          anonymous: boolean,
          media_files: [InpowerProto.MediaFielsInput.t()],
          polls: InpowerProto.PollsInput.t() | nil,
          title: String.t()
        }

  defstruct [:user_id, :group_id, :content, :sensitive, :anonymous, :media_files, :polls, :title]

  field :user_id, 1, type: :string
  field :group_id, 2, repeated: true, type: :string
  field :content, 3, type: :string
  field :sensitive, 4, type: :bool
  field :anonymous, 5, type: :bool
  field :media_files, 6, repeated: true, type: InpowerProto.MediaFielsInput
  field :polls, 7, type: InpowerProto.PollsInput
  field :title, 8, type: :string
end

defmodule InpowerProto.MediaFielsInput do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          file: String.t(),
          height: String.t(),
          width: String.t()
        }

  defstruct [:file, :height, :width]

  field :file, 1, type: :string
  field :height, 2, type: :string
  field :width, 3, type: :string
end

defmodule InpowerProto.PollsInput do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          title: String.t(),
          summary: String.t(),
          options: [String.t()]
        }

  defstruct [:title, :summary, :options]

  field :title, 1, type: :string
  field :summary, 2, type: :string
  field :options, 3, repeated: true, type: :string
end

defmodule InpowerProto.PostResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          active: boolean,
          anonymous: boolean,
          content: String.t(),
          group_id: [String.t()],
          id: String.t(),
          user_id: String.t(),
          media: [InpowerProto.MediaResponse.t()],
          polls: InpowerProto.PollsResponse.t() | nil,
          sensitive: boolean,
          title: String.t(),
          updated_at: String.t(),
          inserted_at: String.t()
        }

  defstruct [
    :active,
    :anonymous,
    :content,
    :group_id,
    :id,
    :user_id,
    :media,
    :polls,
    :sensitive,
    :title,
    :updated_at,
    :inserted_at
  ]

  field :active, 1, type: :bool
  field :anonymous, 2, type: :bool
  field :content, 3, type: :string
  field :group_id, 4, repeated: true, type: :string
  field :id, 5, type: :string
  field :user_id, 6, type: :string
  field :media, 7, repeated: true, type: InpowerProto.MediaResponse
  field :polls, 8, type: InpowerProto.PollsResponse
  field :sensitive, 9, type: :bool
  field :title, 10, type: :string
  field :updated_at, 11, type: :string
  field :inserted_at, 12, type: :string
end

defmodule InpowerProto.MediaResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t(),
          src: String.t(),
          type: InpowerProto.MediaResponse.MediaType.t(),
          order: integer,
          caption: String.t(),
          active: boolean,
          post_id: String.t(),
          height: String.t(),
          width: String.t()
        }

  defstruct [:id, :src, :type, :order, :caption, :active, :post_id, :height, :width]

  field :id, 1, type: :string
  field :src, 2, type: :string
  field :type, 3, type: InpowerProto.MediaResponse.MediaType, enum: true
  field :order, 4, type: :int32
  field :caption, 5, type: :string
  field :active, 6, type: :bool
  field :post_id, 7, type: :string
  field :height, 8, type: :string
  field :width, 9, type: :string
end

defmodule InpowerProto.PollsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          active: boolean,
          id: String.t(),
          poll_options: [InpowerProto.PollOptionsResponse.t()],
          post_id: String.t(),
          summary: String.t(),
          title: String.t()
        }

  defstruct [:active, :id, :poll_options, :post_id, :summary, :title]

  field :active, 1, type: :bool
  field :id, 2, type: :string
  field :poll_options, 3, repeated: true, type: InpowerProto.PollOptionsResponse
  field :post_id, 4, type: :string
  field :summary, 5, type: :string
  field :title, 6, type: :string
end

defmodule InpowerProto.PollOptionsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          active: boolean,
          context: String.t(),
          id: String.t(),
          polls_id: String.t(),
          poll_votes: [InpowerProto.PollVotesResponse.t()]
        }

  defstruct [:active, :context, :id, :polls_id, :poll_votes]

  field :active, 1, type: :bool
  field :context, 2, type: :string
  field :id, 3, type: :string
  field :polls_id, 4, type: :string
  field :poll_votes, 5, repeated: true, type: InpowerProto.PollVotesResponse
end

defmodule InpowerProto.PollVotesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t(),
          active: boolean,
          user_id: String.t(),
          context: String.t(),
          poll_options_id: String.t()
        }

  defstruct [:id, :active, :user_id, :context, :poll_options_id]

  field :id, 1, type: :string
  field :active, 2, type: :bool
  field :user_id, 3, type: :string
  field :context, 4, type: :string
  field :poll_options_id, 5, type: :string
end

defmodule InpowerProto.ListPostsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          posts: [InpowerProto.PostResponse.t()]
        }

  defstruct [:posts]

  field :posts, 1, repeated: true, type: InpowerProto.PostResponse
end

defmodule InpowerProto.PostResponsePaged do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          has_next: boolean,
          has_prev: boolean,
          prev_page: integer,
          list: [InpowerProto.PostResponse.t()],
          next_page: integer,
          first_page: integer,
          page: integer
        }

  defstruct [:has_next, :has_prev, :prev_page, :list, :next_page, :first_page, :page]

  field :has_next, 1, type: :bool
  field :has_prev, 2, type: :bool
  field :prev_page, 3, type: :int32
  field :list, 4, repeated: true, type: InpowerProto.PostResponse
  field :next_page, 5, type: :int32
  field :first_page, 6, type: :int32
  field :page, 7, type: :int32
end

defmodule InpowerProto.ListPostsResponsePagination do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          posts: InpowerProto.PostResponsePaged.t() | nil
        }

  defstruct [:posts]

  field :posts, 1, type: InpowerProto.PostResponsePaged
end

defmodule InpowerProto.PostRequestInput do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t()
        }

  defstruct [:id]

  field :id, 1, type: :string
end

defmodule InpowerProto.UsersPostRequestInput do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_id: String.t(),
          page_no: integer,
          per_page: integer
        }

  defstruct [:user_id, :page_no, :per_page]

  field :user_id, 1, type: :string
  field :page_no, 2, type: :int32
  field :per_page, 3, type: :int32
end

defmodule InpowerProto.GroupsPostRequestInput do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          group_id: String.t(),
          page_no: integer,
          per_page: integer
        }

  defstruct [:group_id, :page_no, :per_page]

  field :group_id, 1, type: :string
  field :page_no, 2, type: :int32
  field :per_page, 3, type: :int32
end

defmodule InpowerProto.UpdatePostRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          post_id: String.t(),
          group_id: [String.t()],
          content: String.t(),
          sensitive: boolean,
          anonymous: boolean,
          media_files: [InpowerProto.MediaFielsInput.t()],
          deleted_media: [String.t()],
          title: String.t()
        }

  defstruct [
    :post_id,
    :group_id,
    :content,
    :sensitive,
    :anonymous,
    :media_files,
    :deleted_media,
    :title
  ]

  field :post_id, 1, type: :string
  field :group_id, 2, repeated: true, type: :string
  field :content, 3, type: :string
  field :sensitive, 4, type: :bool
  field :anonymous, 5, type: :bool
  field :media_files, 6, repeated: true, type: InpowerProto.MediaFielsInput
  field :deleted_media, 7, repeated: true, type: :string
  field :title, 8, type: :string
end

defmodule InpowerProto.UpdatePostResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: String.t(),
          message: String.t()
        }

  defstruct [:status, :message]

  field :status, 1, type: :string
  field :message, 2, type: :string
end

defmodule InpowerProto.DeletePostRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t()
        }

  defstruct [:id]

  field :id, 1, type: :string
end

defmodule InpowerProto.DeletePostResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: String.t(),
          message: String.t()
        }

  defstruct [:status, :message]

  field :status, 1, type: :string
  field :message, 2, type: :string
end

defmodule InpowerProto.DeletePollRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t()
        }

  defstruct [:id]

  field :id, 1, type: :string
end

defmodule InpowerProto.DeletePollResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: String.t(),
          message: String.t()
        }

  defstruct [:status, :message]

  field :status, 1, type: :string
  field :message, 2, type: :string
end

defmodule InpowerProto.Empty do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end
defmodule InpowerProto.Posts.Service do
  @moduledoc false
  use GRPC.Service, name: "inpower_proto.Posts"

  rpc :Create, InpowerProto.CreateRequestInput, InpowerProto.PostResponse

  rpc :GetPost, InpowerProto.PostRequestInput, InpowerProto.PostResponse

  rpc :GetUsersPost, InpowerProto.UsersPostRequestInput, InpowerProto.ListPostsResponsePagination

  rpc :GetGroupsPost,
      InpowerProto.GroupsPostRequestInput,
      InpowerProto.ListPostsResponsePagination

  rpc :GetPosts, InpowerProto.ListPostsRequest, InpowerProto.ListPostsResponsePagination

  rpc :UpdatePost, InpowerProto.UpdatePostRequest, InpowerProto.UpdatePostResponse

  rpc :DeletePost, InpowerProto.DeletePostRequest, InpowerProto.DeletePostResponse

  rpc :DeletePoll, InpowerProto.DeletePollRequest, InpowerProto.DeletePollResponse
end

defmodule InpowerProto.Posts.Stub do
  @moduledoc false
  use GRPC.Stub, service: InpowerProto.Posts.Service
end
