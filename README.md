# InpowerProto

**Inpower: Applications Protobuf**

## Installation

```elixir
def deps do
  [
    {:inpower_proto, "~> 0.1.0"}
  ]
end
```

**TO MAKE Elixir PROTO run the below command**
`protoc --elixir_out=plugins=grpc:./lib/ *.proto`


